#!/bin/bash

if [ $# -ne 1 ]; then
        echo -e "Usage: $0 [TAPE BARCODE]\n";
	echo -e "You can use psql LIKE wildcards to hunt down tapes\n";
	echo -e "  %\tallows you to match any string of any length (including zero length)";
	echo -e "  _\tallows you to match on a single character\n";
        exit
fi

bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT DISTINCT 
job.jobid,job.job,job.name,job.endtime,job.jobfiles,job.jobbytes
FROM media,pool,jobmedia,job
WHERE job.jobid=jobmedia.jobid AND jobmedia.mediaid=media.mediaid AND pool.poolid=media.poolid AND media.volumename LIKE '$1'
ORDER BY job.endtime DESC;
.
quit
EOF
";

