#!/bin/bash
# Bacula reschedule-job.sh 
# Cancel all waiting and running jobs with this script.Use it with cron
# Copyright (C) 2011  Fabian Chong
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

if [ $# -ne 1 ]; then
        echo "Usage: $0 [PREVIOUS_JOBID]";
        echo "";
        echo "This script will grab information from previous job and run it now";
        exit
fi


# Run job based on jobid
function run_job {
bash -c "/usr/sbin/bconsole<<EOF
run level=$1 pool=$2 yes job="$3"
.
quit
EOF";
}

JOB=$(bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT name
FROM job
WHERE jobid='$1';
.
quit
EOF" | gawk -F\| '{ printf "%s",$2 }' | gawk '{ print $2 }' )

LEVELID=$(bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT level
FROM job   
WHERE jobid='$1';
.
quit
EOF" | gawk -F\| '{ printf "%s",$2 }' | gawk '{ print $2 }' )

POOL=$(bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT pool.name
FROM job,pool
WHERE pool.poolid=job.poolid AND jobid='$1';
.
quit
EOF" | gawk -F\| '{ printf "%s",$2 }' | gawk '{ print $2 }' )

case "$LEVELID" in

I) 	LEVEL="Incremental"
	;;
D)	LEVEL="Differential"
	;;
F)	LEVEL="Full"
	;;
B)	LEVEL="Base"
	;;
S)	LEVEL="Since"
	;;
esac

echo $JOB

run_job $LEVEL $POOL "$JOB";

