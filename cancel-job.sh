#!/bin/bash
# Bacula cancel_job.sh 
# Cancel all waiting and running jobs with this script.Use it with cron
# Copyright (C) 2011  Fabian Chong
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

# Cancel job based on jobid
function cancel_job {
bash -c "/usr/sbin/bconsole<<EOF
cancel jobid=$1
.
quit
EOF";
}

if [ $# -ne 1 ]; then
        echo "Usage: $0 [[YYYY-MM-DD][STRING]]";
        echo "";
        echo "Query all bacula jobs between STRING 00:00:00 to 23:59:59"
        echo "Could display date described by STRING like in date function";
        exit
fi

TODAY=$(date +%Y-%m-%d --date="$1");
#YESTERDAY=$(date +%Y-%m-%d --date="$TODAY -1 day")

# Get List of Job schedule to run or running
# Arrange that running job are last
# Exclude BackupCatalog from being cancel
JOBID=$(bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT jobid,name,type,level,jobstatus,schedtime,starttime,realendtime
FROM job
WHERE (jobstatus='C' OR jobstatus='R')
AND name!='BackupCatalog'
AND schedtime between '$TODAY 00:00:00' AND '$TODAY 23:59:59'
ORDER BY jobstatus ASC;
.
quit
EOF" | gawk -F\| '{ print $2 }' | sed 's/,//g' )

for t in $JOBID
do
        if [ $t != 'jobid' ]; then
                echo "Cancelling jobid $t"
		cancel_job $t
        fi
done
