#!/bin/bash

if [ $# -lt 1 ]; then
        echo -e "Usage: $0 [Pool Name]\n";
	echo -e "You can use psql LIKE wildcards to hunt down tapes\n";
	echo -e "  %\tallows you to match any string of any length (including zero length)";
	echo -e "  _\tallows you to match on a single character\n";
        exit
fi

bash -c "/usr/sbin/bconsole<<EOF
update slots au-mel-build-1-autochanger-1
sqlquery
SELECT media.mediaid,media.volumename,media.volstatus,pool.name 
AS pool,media.mediatype,media.lastwritten
FROM media,pool
WHERE pool.name LIKE '$1%' AND pool.poolid=media.poolid  
ORDER BY media.poolid
ASC,media.lastwritten DESC;
.
quit
EOF
";
