#!/bin/bash

if [ $# -ne 1 ]; then
        echo "Usage: $0 [[YYYY-MM-DD][STRING]]";
	echo "";
        echo "Query all bacula jobs between given date from 00:00:00 to 23:59:59"
	echo "Can also accept STRING like in date function";
        exit
fi

#if [ $1 = "now" ] || [ $1 = "today" ]; then
	FIRSTDATE=$(date +%Y-%m-%d --date="$1");
#        SECONDDATE=$(date +%Y-%m-%d --date="$FIRSTDATE next day");
#else
#	FIRSTDATE=$1;
#	SECONDDATE=$(date +%Y-%m-%d --date="$1 next day");
#fi

echo "###########################################################";
echo "#  Displaying all jobs between $FIRSTDATE and $SECONDDATE  #";
echo '###########################################################';
echo '';
echo '';
echo '';

bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT
jobid,name,type,level,status.jobstatuslong AS status,starttime,endtime,jobfiles AS files,round( (jobbytes/1073741824.00) ,2 ) AS
jobGigabytes
FROM Job,status
WHERE status.jobstatus=job.jobstatus
AND schedtime between '$FIRSTDATE 00:00:00' AND '$FIRSTDATE 23:59:59'
ORDER BY starttime DESC;
.
quit
EOF
";

