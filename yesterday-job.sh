#!/bin/bash

TODAY=$(date +%Y-%m-%d)
YESTERDAY=$(date +%Y-%m-%d --date="yesterday")

bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT 
jobid,name,type,level,status.jobstatuslong AS status,starttime,endtime,jobfiles AS files,round( (jobbytes/1073741824.00) ,2 ) AS 
jobGigabytes
FROM Job,status
WHERE status.jobstatus=job.jobstatus
AND starttime IS NOT NULL 
AND schedtime between '$YESTERDAY 00:00:00' AND '$TODAY 23:59:59'
ORDER BY starttime DESC;
.
quit
EOF
";

