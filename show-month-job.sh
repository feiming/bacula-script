#!/bin/bash

if [ $# -ne 2 ]; then
        echo "Usage: $0 [YYYY-MM-DD] [YYYY-MM-DD]";
	echo "";
        echo "Query all bacula jobs between given date from 00:00:00 to 23:59:59"
	echo "Can also accept STRING like in date function";
        exit
fi

STARTMONTH=$(date +%Y-%m-%d --date="$1");
ENDMONTH=$(date +%Y-%m-%d --date="$2");


echo "###########################################################";
echo "#  Displaying all jobs between $STARTMONTH and $ENDMONTH  #";
echo '###########################################################';
echo '';
echo '';
echo '';

bash -c "/usr/sbin/bconsole<<EOF
sqlquery
SELECT
jobid,name,type,level,status.jobstatuslong AS status,starttime,endtime,jobfiles AS files,round( (jobbytes/1073741824.00) ,2 ) AS
jobGigabytes
FROM Job,status
WHERE status.jobstatus=job.jobstatus
AND name LIKE '%month%'
AND schedtime between '$STARTMONTH 00:00:00' AND '$ENDMONTH 23:59:59'
ORDER BY starttime DESC;
.
quit
EOF
";

